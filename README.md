# wrs-flask

#### 介绍

flaks 废品回收系统（学习使用）

#### 软件架构

软件架构说明：略

#### 准备工作

1. Pycharm安装
   （破解教程：https://blog.idejihuo.com/jetbrains/intellij-idea-2023-3-cracking-tutorial-latest-activation-code-family-bucket-activation.html）
2. MySQL安装（>=8.0版本，建议本地安装）（略）
3. Redis安装（建议本地安装，略）
4. Python安装（>=3.10.0版本），建议使用Pycharm安装python环境
5. （可忽略）本来有GO环境的可以使用Makefile中命令：make init, make api生成openapi.yaml（用于swagger）

#### 使用说明

1. 安装依赖：pip install -r requirements.txt
2. 更改配置app/config.py中的配置
    1. 更改mysql：host、端口、db、密码、用户（建议root）
    2. 执行doc下wms.sql或wms-flask.sql(区别在于，前者未初始化数据，后者初始化数据)
    3. 更改redis配置：host、端口、（密码：可选），db
3. 运行：python -m flask run或制定host和端口python -m flask run -h 192.168.10.12 -p 5000
4. doc中包含api.proto（接口文档）, 其中resp部分定义的响应体中的data
5. 127.0.0.1:5000/api/docs

```text
http resp：
{
    code:0,     // 0为正常，其他为异常code
    msg:"",     // 正常为空，异常值为错误提示
    data: {}    // 响应数据
}
```

