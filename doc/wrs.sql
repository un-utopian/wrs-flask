-- 用户表
CREATE TABLE `wrs_user`
(
    `id`          int          NOT NULL AUTO_INCREMENT,
    `username`    varchar(32)  NOT NULL DEFAULT '' COMMENT '用户名',
    `phone`       varchar(32)  NOT NULL DEFAULT '' COMMENT '手机号',
    `email`       varchar(32)  NOT NULL DEFAULT '' COMMENT '邮箱',
    `password`    varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
    `is_del`      tinyint      NOT NULL DEFAULT 0 COMMENT '是否下线 0-正常 1-删除',
    `create_time` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='用户';

-- 角色表
CREATE TABLE `wrs_role`
(
    `id`          int         NOT NULL AUTO_INCREMENT,
    `role_name`   varchar(32) NOT NULL DEFAULT '' COMMENT '角色名称',
    `role_code`   varchar(32) NOT NULL DEFAULT '' COMMENT '角色编码',
    `is_del`      tinyint     NOT NULL DEFAULT 0 COMMENT '是否下线 0-正常 1-删除',
    `create_time` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `udx_role_code` (`role_code`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='角色';

-- 用户角色表
CREATE TABLE `wrs_r_user_role`
(
    `id`          int      NOT NULL AUTO_INCREMENT,
    `user_id`     int      NOT NULL DEFAULT 0 COMMENT '用户id',
    `role_id`     int      NOT NULL DEFAULT 0 COMMENT '角色id',
    `operator`    int      NOT NULL DEFAULT 0 COMMENT '操作人',
    `is_del`      tinyint  NOT NULL DEFAULT 0 COMMENT '是否下线 0-正常 1-删除',
    `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `idx_user_role` (`user_id`, `role_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='用户-角色';

-- 废品大类表
CREATE TABLE `wrs_waste_type`
(
    `id`              int         NOT NULL AUTO_INCREMENT,
    `waste_type_name` varchar(32) NOT NULL DEFAULT '' COMMENT '废品类别名',
    `is_del`          tinyint     NOT NULL DEFAULT 0 COMMENT '是否下线 0-正常 1-删除',
    `create_time`     datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time`     datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `udx_waste_type_name` (`waste_type_name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='废品大类表';

-- 回收区域表
CREATE TABLE `wrs_recycling_area`
(
    `id`          int         NOT NULL AUTO_INCREMENT,
    `area_name`   varchar(32) NOT NULL DEFAULT '' COMMENT '区域名称',
    `area_type`   tinyint     NOT NULL DEFAULT 0 COMMENT '区域类别：1-教学区域 2-生活区域 3-后勤区域 4-办公区域 5-其他区域',
    `is_del`      tinyint     NOT NULL DEFAULT 0 COMMENT '是否下线 0-正常 1-删除',
    `create_time` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `udx_area_name` (`area_name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='回收区域表';

-- 订单表
CREATE TABLE `wrs_recycling_order`
(
    `id`                int          NOT NULL AUTO_INCREMENT,
    `username`          varchar(32)  NOT NULL DEFAULT '' COMMENT '下单人员名称',
    `phone`             varchar(32)  NOT NULL DEFAULT '' COMMENT '下单电话',
    `appointment_time`  datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '预约上门时间',
    `recycling_area_id` int          NOT NULL DEFAULT 0 COMMENT '回收区域ID',
    `waste_type_id`     int          NOT NULL DEFAULT 0 COMMENT '废品大类ID',
    `address`           varchar(64)  NOT NULL DEFAULT '' COMMENT '详细地址：如10号楼几零几',
    `remark`            varchar(128) NOT NULL DEFAULT '' COMMENT '备注',
    `status`            tinyint      NOT NULL DEFAULT 0 COMMENT '订单状态： 0-创建 1-待接单 2-待上门 3-已完成 4-取消订单',
    `is_del`            tinyint      NOT NULL DEFAULT 0 COMMENT '是否下线 0-正常 1-删除',
    `create_time`       datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time`       datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `idx_waste_type_id` (`waste_type_id`),
    INDEX `idx_recycling_area_id` (`recycling_area_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='回收订单表';

-- 回收接单表
CREATE TABLE `wrs_take_order`
(
    `id`                int      NOT NULL AUTO_INCREMENT,
    `ragman_id`         int      NOT NULL DEFAULT 0 COMMENT '废品回收人员ID',
    `order_id`          int      NOT NULL DEFAULT 0 COMMENT '订单ID',
    `transaction_price` int      NOT NULL DEFAULT 0 COMMENT '成交价格，单位分',
    `turnover`          int      NOT NULL DEFAULT 0 COMMENT '成交量，单位克',
    `completion_time`   datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '完成时间',
    `is_del`            tinyint  NOT NULL DEFAULT 0 COMMENT '是否下线 0-正常 1-删除',
    `create_time`       datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '接单时间',
    `update_time`       datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `idx_take_order` (`ragman_id`, `order_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='回收接单表';

-- 废品报价表
CREATE TABLE `wrs_r_waste_quotation_price`
(
    `id`              int      NOT NULL AUTO_INCREMENT,
    `waste_type_id`   int      NOT NULL DEFAULT 0 COMMENT '废品类别ID',
    `recycling_price` int      NOT NULL DEFAULT 0 COMMENT '回收价格，单位（分）',
    `quotation_date`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '报价日期',
    `is_del`          tinyint  NOT NULL DEFAULT 0 COMMENT '是否下线 0-正常 1-删除',
    `create_time`     datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '接单时间',
    `update_time`     datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `idx_daily_quotation` (`waste_type_id`, `quotation_date`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='废品报价表';


-- 数据初始化
INSERT INTO `wrs_role` (`id`, `role_name`, `role_code`)
VALUES (1, '系统管理员', 'admin'),
       (2, '废品回收人员', 'ragman'),
       (3, '普通用户', 'ordinary_user');


INSERT INTO `wrs_user` (`id`, `username`, `email`, `password`)
VALUES (1, 'admin', 'admin@qq.com',
        '$pbkdf2-sha256$29000$KsVYq3WulRJi7D2HEEKo1Q$ZoX/ym8K97Ecblo3BiN85ReR7aUGlrKScrJYudXvUEc');


INSERT INTO `wrs_r_user_role` (`id`, `user_id`, `role_id`, `operator`)
VALUES (1, 1, 1, 0);


INSERT INTO `wrs_waste_type`(`waste_type_name`)
VALUES ('铁'),
       ('塑料'),
       ('玻璃'),
       ('纸类'),
       ('衣服类');

INSERT INTO wrs_recycling_area (`area_name`, `area_type`)
VALUES ('产教融合大楼', 1),
       ('公教楼', 1),
       ('图书馆', 1),
       ('音美体楼', 1),
       ('宿舍楼', 2),
       ('后勤楼', 3),
       ('第一食堂', 3),
       ('第二食堂', 3),
       ('理科楼', 4),
       ('文科楼', 4),
       ('行政楼', 4),
       ('操场', 5);
