# MySQL 相关的配置信息
from datetime import timedelta
from flask_swagger_ui import get_swaggerui_blueprint

MYSQL_DATABASE = "wrs_flask"  # 填写自己的db
MYSQL_HOST = "127.0.0.1"
MYSQL_PORT = 3306
MYSQL_USER = "root"
MYSQL_PASSWORD = "zongjiye"  # 填写密码
MYSQL_MAX_CONNECTIONS = 10

# Flask配置
DEBUG = True
# 如果没有密码的redis使用redis://127.0.0.1:6379/1
REDIS_URL = "redis://:zongjiye@127.0.0.1:6379/1"
JWT_SECRET_KEY = "super-secret"
JWT_HEADER_NAME = "token"
JWT_HEADER_TYPE = ""
JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=7)

# 日志配置
LOG_FILE = "logs/wms-{time}.log"
LOG_LEVEL = "DEBUG"
LOG_RETENTION = 7
LOG_ROTATION = "12:00"

# swagger 配置
APP_NAME = "废品回收系统"
SWAGGER_URL = '/api/docs'
API_URL = '/static/openapi.yaml'
SWAGGER_UI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "废品回收系统"
    }
)
