from flask import Flask
from flask_cors import CORS
from . import models, routes
from loguru import logger
import logging
import sys


# create a custom handler
class InterceptHandler(logging.Handler):
    def emit(self, record):
        logger_opt = logger.opt(depth=6, exception=record.exc_info)
        logger_opt.log(record.levelname, record.getMessage())


def create_app():
    app = Flask(__name__, static_folder='static')
    CORS(app, resources=r'/*')
    app.config.from_pyfile('config.py')

    # 初始化日志
    init_logger(app)
    # 注册路由
    routes.register_routes(app)
    # 初始化数据库
    models.init_database(app)

    return app


def init_logger(app):
    logger.remove()
    # 初始化日志
    logger.add(
        app.config["LOG_FILE"],
        retention=app.config["LOG_RETENTION"],
        rotation=app.config["LOG_ROTATION"],
        encoding="utf-8",
        level=app.config["LOG_LEVEL"],
    )
    # 设置标准输出的日志级别
    logger.add(sys.stdout, level=app.config["LOG_LEVEL"])
    # 注册logger
    app.logger.addHandler(InterceptHandler())
    # 设置数据logger
    if app.config["LOG_LEVEL"] == "DEBUG":
        logging.getLogger("peewee").setLevel(app.config["LOG_LEVEL"])
        logging.getLogger("peewee").addHandler(InterceptHandler())
