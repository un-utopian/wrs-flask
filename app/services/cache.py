from app.models import rdb


class CacheServie:
    def __init__(self):
        self.cache = rdb

    def exists(self, key):
        return self.cache.exists(key) == 1

    def set(self, key, value, ex=None):
        return self.cache.set(key, value, ex)

    def get(self, key):
        bts: bytes = self.cache.get(key)
        if bts is not None:
            return bts.decode('utf-8')

        return ""
