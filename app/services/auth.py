from app.services.base import BaseService
from app.models.user import *


class AuthService(BaseService):
    def __init__(self):
        super().__init__()

    def list_roles(self, page=1, size=1000, get_all=False) -> list[WrsRole] | None:
        if get_all:
            self.wrs_role.select()
        return self.wrs_role.select().paginate(page, size)

    def get_role_by_code(self, role_code: str) -> WrsRole | None:
        return self.wrs_role.get_or_none(role_code=role_code)

    def get_role_by_id(self, role_id: int) -> WrsRole:
        return self.wrs_role.get_or_none(id=role_id)

    def role_count(self) -> int:
        return self.wrs_role.select().count()
