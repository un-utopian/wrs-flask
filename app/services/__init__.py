from app.services.auth import AuthService
from app.services.user import UserService
from app.services.cache import CacheServie
from app.services.waste_recycling import WasteRecyclingService

user_srv = UserService()
cache = CacheServie()
auth_srv = AuthService()
wr_srv = WasteRecyclingService()
