from app.models.user import *
from app.models.waste_recycling import *
from datetime import datetime


class BaseService(object):
    def __init__(self):
        self.wrs_user = WrsUser
        self.wrs_role = WrsRole
        self.wrs_user_role = WrsUserRole
        self.wrs_waste_type = WrsWasteType
        self.wrs_recycling_area = WrsRecyclingArea
        self.wrs_waste_quotation_price = WrsWasteQuotationPrice
        self.wrs_recycling_order = WrsRecyclingOrder
        self.wrs_take_order = WrsTakeOrder

    @staticmethod
    def parse_time(date_string: str, date_format: str):
        return datetime.strptime(date_string, date_format)

    # ml中的每一项必须包含id字段
    @staticmethod
    def list_to_dict(model_list: list) -> dict:
        if len(model_list) == 0:
            return {}
        if not hasattr(model_list[0], 'id'):
            return {}
        return {model.id: model for model in model_list}
