from app.services.base import BaseService
from app.models.waste_recycling import *
from app.forms.forms import *
from app.models import db
from app.models.enum import RecyclingOrderStatus
from datetime import datetime
from loguru import logger


class WasteRecyclingService(BaseService):
    def __init__(self):
        super().__init__()

    def get_waste_type(self, type_id: int) -> WrsWasteType | None:
        return self.wrs_waste_type.get_or_none(id=type_id)

    def list_waste_types(self) -> list[WrsWasteType]:
        return self.wrs_waste_type.select().execute()

    def get_recycling_area(self, area_id: int) -> WrsRecyclingArea | None:
        return self.wrs_recycling_area.get_or_none(id=area_id)

    def list_recycling_areas(self) -> list[WrsRecyclingArea]:
        return self.wrs_recycling_area.select().execute()

    def get_today_waste_type_price(self, waste_type_id: int) -> WrsWasteQuotationPrice | None:
        # 获取当前日期时间
        today = datetime.now().date()
        # 将时间设为零点
        return self.wrs_waste_quotation_price.get_or_none(
            (WrsWasteQuotationPrice.waste_type_id == waste_type_id) &
            (WrsWasteQuotationPrice.quotation_date >= today)
        )

    def create_today_waste_type_price(self, form: WasteTypePriceForm) -> WrsWasteQuotationPrice | None:
        return self.wrs_waste_quotation_price.create(
            waste_type_id=form.get_waste_type_id(),
            recycling_price=form.get_today_price(),
            quotation_date=datetime.now(),
        )

    def update_waste_type_price(self, form: WasteTypePriceForm) -> int:
        price = self.get_today_waste_type_price(form.get_waste_type_id())
        if price:
            price.recycling_price = form.get_today_price()
            if price.save() <= 0:
                return ErrCode.ERROR_INTERNAL_SERVER_ERROR

            return ErrCode.ERROR_SUCCESS
        if self.create_today_waste_type_price(form) is None:
            return ErrCode.ERROR_INTERNAL_SERVER_ERROR

        return ErrCode.ERROR_SUCCESS

    def list_waste_type_prices(self, get_today=False) -> list[WrsWasteQuotationPrice]:
        q = self.wrs_waste_quotation_price.select()
        # 根据过滤条件构建查询
        if get_today:
            # 获取当前日期时间
            today = datetime.now().date()
            q = q.where(WrsWasteQuotationPrice.quotation_date >= today)
        return q.execute()

    def create_take_order(self, order_id: int) -> WrsTakeOrder | None:
        return self.wrs_take_order.create(
            order_id=order_id,
        )

    def get_take_order(self, order_id: int) -> WrsTakeOrder | None:
        return self.wrs_take_order.get_or_none(order_id=order_id)

    def create_recycling_order(self, order_form: CreateRecyclingOrderForm) -> WrsRecyclingOrder | None:
        with db.atomic() as txn:
            try:
                order = self.wrs_recycling_order.create(
                    username=order_form.get_username(),
                    phone=order_form.get_phone(),
                    appointment_time=self.parse_time(order_form.get_appointment_time(), "%Y-%m-%d %H:%M"),
                    recycling_area_id=order_form.get_recycling_area_id(),
                    waste_type_id=order_form.get_waste_type_id(),
                    address=order_form.get_address(),
                    remark=order_form.get_remark()
                )
                if order is None:
                    txn.rollback()
                    return None
                if self.create_take_order(order_id=order.id) is None:
                    txn.rollback()
                    return None

                txn.commit()
                return order
            except Exception as e:
                logger.error(e)
                txn.rollback()
                return None

    def list_take_orders(self) -> list[WrsTakeOrder]:
        return self.wrs_take_order.select().execute()

    def get_recycling_order(self, order_id: int) -> WrsRecyclingOrder | None:
        return self.wrs_recycling_order.get_or_none(id=order_id)

    def update_recycling_order(self, update_form: UpdateRecyclingOrderForm) -> int:
        with db.atomic() as txn:
            try:
                order = self.get_recycling_order(update_form.get_recycling_order_id())
                if order is None:
                    txn.rollback()
                    return ErrCode.ERROR_INTERNAL_SERVER_ERROR
                order.username = update_form.get_username()
                order.phone = update_form.get_phone()
                order.appointment_time = update_form.get_appointment_time()
                order.recycling_area_id = update_form.get_recycling_area_id()
                order.waste_type_id = update_form.get_waste_type_id()
                order.address = update_form.get_address()
                order.remark = update_form.get_remark()
                order.status = update_form.get_status()

                take_order = self.get_take_order(order_id=order.id)
                if take_order and update_form.get_ragman_id():
                    take_order.ragman_id = update_form.get_ragman_id()
                    take_order.transaction_price = update_form.get_transaction_price()
                    take_order.turnover = update_form.get_turnover()
                    if update_form.get_status() == RecyclingOrderStatus.COMPLETED.value:
                        take_order.completion_time = datetime.now()

                    if take_order.save() <= 0:
                        txn.rollback()
                        return ErrCode.ERROR_INTERNAL_SERVER_ERROR

                if order.save() <= 0:
                    txn.rollback()
                    return ErrCode.ERROR_INTERNAL_SERVER_ERROR

                txn.commit()
                return ErrCode.ERROR_SUCCESS
            except Exception as e:
                logger.error(e)
                txn.rollback()
                return ErrCode.ERROR_INTERNAL_SERVER_ERROR

    def delete_recycling_order(self, order_id: int) -> int:
        with db.atomic() as txn:
            try:
                self.wrs_recycling_order.delete().where(WrsRecyclingOrder.id == order_id).execute()
                self.wrs_take_order.delete().where(WrsTakeOrder.order_id == order_id).execute()
                txn.commit()

                return ErrCode.ERROR_SUCCESS
            except Exception as e:
                logger.error(e)
                txn.rollback()
                return ErrCode.ERROR_INTERNAL_SERVER_ERROR

    def list_recycling_orders(self) -> list[WrsRecyclingOrder]:
        return self.wrs_recycling_order.select().execute()

    def batch_take_order(self, order_ids, ragman_id):
        self.wrs_take_order.update(ragman_id=ragman_id).where(WrsTakeOrder.order_id.in_(order_ids)).execute()
        return ErrCode.ERROR_SUCCESS

    def update_order_execution_info(self, form: UpdateOrderExecutionInfoRequestForm):
        with db.atomic() as txn:
            try:
                order = self.get_recycling_order(form.get_recycling_order_id())
                if order is None:
                    txn.rollback()
                    return ErrCode.ERROR_RESOURCE_NOT_FOUND

                tk = self.get_take_order(form.get_recycling_order_id())
                if tk is None:
                    txn.rollback()
                    return ErrCode.ERROR_RESOURCE_NOT_FOUND

                tk.transaction_price = form.get_transaction_price()
                tk.turnover = form.get_turnover()
                tk.completion_time = datetime.now()
                order.status = form.get_status()
                if tk.save() <= 0 or order.save() <= 0:
                    txn.rollback()
                    return ErrCode.ERROR_INTERNAL_SERVER_ERROR

                txn.commit()
                return ErrCode.ERROR_SUCCESS
            except Exception as e:
                logger.error(e)
                txn.rollback()
                return ErrCode.ERROR_INTERNAL_SERVER_ERROR
