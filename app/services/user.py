from app.models.user import *
from app.models import db
from app.services.auth import AuthService
from app.models.enum import RoleCode

from peewee import DoesNotExist
from loguru import logger


class UserService(AuthService):
    def __init__(self):
        super().__init__()

    def create(self, username, email, password, phone, role_code=RoleCode.ORDINARY_USER.value) -> WrsUser | None:
        with db.atomic() as txn:
            try:
                user: WrsUser = self.wrs_user.create(username=username, email=email, password=password, phone=phone)
                role: WrsRole = self.wrs_role.get_or_none(role_code=role_code)
                if user is None or role is None:
                    txn.rollback()
                    return None
                if self.wrs_user_role.create(user_id=user.id, role_id=role.id) is None:
                    txn.rollback()
                txn.commit()
                return user
            except Exception as e:
                logger.error(e)
                txn.rollback()

    def delete(self, user_id: int) -> None:
        self.wrs_user.delete().where(WrsUser.id == user_id).execute()

    def get_by_id(self, user_id) -> WrsUser:
        return self.wrs_user.get_or_none(id=user_id)

    def get_by_uni_info(self, email, phone) -> WrsUser:
        return self.wrs_user.get_or_none((WrsUser.email == email) | (WrsUser.phone == phone))

    def exist_by_uni_info(self, email, phone):
        return self.wrs_user.select().where((WrsUser.email == email) | (WrsUser.phone == phone)).exists()

    def list_users(self, page=1, size=1000, get_all=False, fc: dict = None) -> list[WrsUser]:
        q = self.wrs_user.select()
        if not get_all:
            q = q.paginate(page, size)
        if fc is not None:
            q = q.where(WrsUser.id.in_(fc.get("user_ids")))
        return q.execute()

    def user_count(self, fc: dict = None) -> int:
        q = self.wrs_user.select()
        if fc is not None:
            q = q.where(WrsUser.id.in_(fc.get("user_ids")))
        return q.count()

    def get_role_by_user_id(self, user_id: int) -> WrsRole | None:
        user_role: WrsUserRole = self.wrs_user_role.get_or_none(user_id=user_id)
        if not user_role:
            return None
        return self.wrs_role.get_or_none(id=user_role.role_id)

    def create_user_role(self, user_id: int, role_code: str) -> WrsUserRole | None:
        role = self.get_role_by_code(role_code)
        if role is not None:
            return self.wrs_user_role.create(user_id=user_id, role_id=role.id)

    def update_user_role(self, user_id: int, role_id: int):
        try:
            user_role: WrsUserRole = self.wrs_user_role.get(user_id=user_id)
            user_role.role_id = role_id
            return user_role.save()
        except DoesNotExist:
            return 0

    def get_user_role_dict(self, user_ids: list[int]) -> dict[int, WrsRole]:
        roles = self.list_roles(get_all=True)
        roles_dict = {role.id: role for role in roles}

        user_roles: list[WrsUserRole] = self.wrs_user_role.select().where(WrsUserRole.user_id.in_(user_ids))
        user_role_dict = {}
        for v in user_roles:
            if v.role_id not in roles_dict:
                continue
            user_role_dict[v.user_id] = roles_dict[v.role_id]

        return user_role_dict

    def list_users_roles(self, role_id: int = 0, user_id: int = 0) -> list[WrsUserRole]:
        q = self.wrs_user_role.select()
        if role_id > 0:
            q = q.where(WrsUserRole.role_id == role_id)
        if user_id > 0:
            q = q.where(WrsUserRole.user_id == user_id)
        return q.execute()
