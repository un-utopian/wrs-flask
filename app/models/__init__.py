import redis
from flask import Flask
from playhouse.pool import PooledMySQLDatabase
from playhouse.shortcuts import ReconnectMixin
from flask_redis import FlaskRedis
from loguru import logger


class ReconnectMySQLDatabase(ReconnectMixin, PooledMySQLDatabase):
    """
        title: playhouse
        PooledMySQLDatabase: MySQL数据库连接池
        ReconnectMixin: 数据库连接被关闭会重新连接
    """
    pass


db = ReconnectMySQLDatabase('', user='', password='', host='localhost', port=3306)
# noinspection PyTypeChecker
rdb: redis.Redis = FlaskRedis()


# 初始化数据库
# app 为flask app
def init_database(app: Flask) -> None:
    # 使用flask app初始化数据库连接
    db.init(
        app.config['MYSQL_DATABASE'],
        user=app.config['MYSQL_USER'],
        password=app.config['MYSQL_PASSWORD'],
        host=app.config['MYSQL_HOST'],
        port=app.config['MYSQL_PORT'],
        max_connections=app.config['MYSQL_MAX_CONNECTIONS'],
    )
    db.connect(True)
    db.logger = logger
    rdb.init_app(app)

    # 设置 Peewee 的日志级别为 DEBUG
    @app.teardown_appcontext
    def teardown_db(exception=None):
        if exception is not None:
            db.close()
        if not db.is_closed():
            db.close()
