from datetime import datetime
from . import db
from peewee import *


class BaseModel(Model):
    # 创建时间、更新时间、删除时间
    is_del = IntegerField(default=0)  # 使用 BooleanField 表示 tinyint(1)
    create_time = DateTimeField(default=datetime.now)
    update_time = DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        # 判断这是一个新添加的数据还是更新的数据
        if self._pk is not None:
            # 这是一个旧数据
            self.update_time = datetime.now()
        return super().save(*args, **kwargs)

    @classmethod
    def delete(cls, permanently=False):  # permanently表示是否永久删除
        if permanently:
            return super().delete_instance()
        else:
            return super().update(is_del=1)

    def delete_instance(self, permanently=False, recursive=False, delete_nullable=False):
        if permanently:  # 此时这里的self是每一个model对象的实例，白话来说就是一条数据
            return super().delete_instance()
        else:
            self.is_del = 1
            self.save()

    @classmethod
    def select(cls, *fields):
        return super().select(*fields).where(cls.is_del == 0)

    class Meta:
        database = db
