from peewee import *
from .base import BaseModel


# 用户表
class WrsUser(BaseModel):
    id = AutoField(primary_key=True)
    username = CharField(max_length=32)
    email = CharField(max_length=32)
    phone = CharField(max_length=32)
    password = CharField(max_length=32)

    class Meta:
        table_name = 'wrs_user'


# 角色表
class WrsRole(BaseModel):
    id = AutoField(primary_key=True)
    role_name = CharField(max_length=32)
    role_code = CharField(max_length=32)

    class Meta:
        table_name = 'wrs_role'


# 用户角色表
class WrsUserRole(BaseModel):
    id = AutoField(primary_key=True)
    user_id = IntegerField()
    role_id = IntegerField()
    operator = CharField(max_length=32)

    class Meta:
        table_name = 'wrs_r_user_role'
