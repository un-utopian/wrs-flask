from peewee import *
from .base import BaseModel


class WrsWasteType(BaseModel):
    id = AutoField(primary_key=True)
    waste_type_name = CharField(max_length=32)

    class Meta:
        table_name = 'wrs_waste_type'


class WrsRecyclingArea(BaseModel):
    id = AutoField(primary_key=True)
    area_name = CharField(max_length=32)
    area_type = SmallIntegerField(default=0)

    class Meta:
        table_name = 'wrs_recycling_area'


class WrsRecyclingOrder(BaseModel):
    id = AutoField(primary_key=True)
    username = CharField(max_length=32)
    phone = CharField(max_length=32)
    appointment_time = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])
    recycling_area_id = IntegerField(default=0)
    waste_type_id = IntegerField(default=0)
    address = CharField(max_length=64)
    remark = CharField(max_length=128)
    status = SmallIntegerField(default=0)

    class Meta:
        table_name = 'wrs_recycling_order'


class WrsTakeOrder(BaseModel):
    id = AutoField(primary_key=True)
    ragman_id = IntegerField(default=0)
    order_id = IntegerField(default=0)
    transaction_price = IntegerField(default=0)
    turnover = IntegerField(default=0)
    completion_time = DateTimeField(SQL('DEFAULT CURRENT_TIMESTAMP'))

    class Meta:
        table_name = 'wrs_take_order'


class WrsWasteQuotationPrice(BaseModel):
    id = AutoField(primary_key=True)
    waste_type_id = IntegerField(default=0)
    recycling_price = IntegerField(default=0)
    quotation_date = DateTimeField(constraints=[SQL('DEFAULT CURRENT_TIMESTAMP')])

    class Meta:
        table_name = 'wrs_r_waste_quotation_price'
