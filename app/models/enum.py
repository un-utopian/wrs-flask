from enum import Enum


class RoleCode(Enum):
    ADMIN = "admin"
    RAGMAN = "ragman"
    ORDINARY_USER = "ordinary_user"


class RecyclingOrderStatus(Enum):
    PENDING = 1
    WAITING_FOR_DOORSTEP = 2
    COMPLETED = 3
    CANCEL = 4
