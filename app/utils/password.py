from passlib.hash import pbkdf2_sha256


class Password:
    @staticmethod
    def encrypt(password):
        # 使用pbkdf2_sha256算法对密码进行加密
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def verify(password, hashed_password):
        # 校验密码是否匹配
        return pbkdf2_sha256.verify(password, hashed_password)


# 示例用法
if __name__ == '__main__':
    plain_password = "password123"
    encrypy_password = Password.encrypt(plain_password)
    print("加密后的密码:", encrypy_password, type(encrypy_password))

    # 校验密码
    print(Password.verify("password123", encrypy_password))  # 输出：True
    print(Password.verify("wrongpassword", encrypy_password))  # 输出：False
