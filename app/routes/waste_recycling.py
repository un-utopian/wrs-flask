from flask import Blueprint
from flask_jwt_extended import jwt_required, current_user
from loguru import logger

from .resp_data import *
from app.forms.forms import *
from app.services import wr_srv
from app.services import user_srv
from app.utils.httpx import HTTPHelper
from app.utils.errcode import ErrCode

# 创建蓝图
recycling_bp = Blueprint('recycling', __name__)
area_bp = Blueprint('area', __name__)
waste_type_bp = Blueprint('waste_type', __name__)
dashboard_bp = Blueprint('dashboard', __name__)


@area_bp.route("/list", methods=["GET"])
@jwt_required()
@logger.catch
def list_recycling_areas():
    areas = wr_srv.list_recycling_areas()
    return HTTPHelper.generate_response(
        data=ListRecyclingAreaData([RecyclingAreaData(v) for v in areas]).to_dict())


@waste_type_bp.route("/list", methods=["GET"])
@jwt_required()
@logger.catch
def list_waste_types():
    tps = wr_srv.list_waste_types()
    prices = wr_srv.list_waste_type_prices(get_today=True)
    price_dict = {x.waste_type_id: x for x in prices}
    data: list[WasteTypeData] = [WasteTypeData(
        v,
        price_dict.get(v.id),
    ) for v in tps]
    return HTTPHelper.generate_response(data=ListWasteTypesResponseData(data).to_dict())


@waste_type_bp.route('/price/update', methods=['POST'])
@jwt_required()
@logger.catch
def update_waste_type_price():
    form = WasteTypePriceForm()
    if not form.validate():
        return form.arguments_error()
    if wr_srv.get_waste_type(form.get_waste_type_id()) is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_VALIDATION_ERROR, msg="废品类型不存在")

    return HTTPHelper.generate_response(code=wr_srv.update_waste_type_price(form))


@recycling_bp.route('/order/create', methods=['POST'])
@jwt_required()
@logger.catch
def create_recycling_order():
    form = CreateRecyclingOrderForm()
    if not form.validate():
        return form.arguments_error()

    if wr_srv.get_waste_type(form.get_waste_type_id()) is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_VALIDATION_ERROR, msg="废品类型不存在")
    if wr_srv.get_recycling_area(form.get_recycling_area_id()) is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_VALIDATION_ERROR, msg="回收区域不存在")

    order = wr_srv.create_recycling_order(order_form=form)
    if order is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_INTERNAL_SERVER_ERROR)

    return HTTPHelper.generate_response()


@recycling_bp.route('/order/update', methods=['POST'])
@jwt_required()
@logger.catch
def update_recycling_order():
    up_form = UpdateRecyclingOrderForm()
    if not up_form.validate():
        return up_form.arguments_error()

    if wr_srv.get_waste_type(up_form.get_waste_type_id()) is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_VALIDATION_ERROR, msg="废品类型不存在")
    if wr_srv.get_recycling_area(up_form.get_recycling_area_id()) is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_VALIDATION_ERROR, msg="回收区域不存在")
    if up_form.get_ragman_id() > 0 and user_srv.get_by_id(up_form.get_ragman_id()) is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_VALIDATION_ERROR, msg="废品回收人员不存在")

    return HTTPHelper.generate_response(code=wr_srv.update_recycling_order(update_form=up_form))


@recycling_bp.route('/order/delete', methods=['POST'])
@jwt_required()
@logger.catch
def delete_recycling_order():
    form = RecyclingOrderIDForm()
    if not form.validate():
        return form.arguments_error()

    wr_srv.delete_recycling_order(order_id=form.get_recycling_order_id())
    return HTTPHelper.generate_response()


@recycling_bp.route('/order/list', methods=['GET'])
@jwt_required()
@logger.catch
def list_recycling_orders():
    orders = wr_srv.list_recycling_orders()
    t_orders = wr_srv.list_take_orders()
    waste_tps = wr_srv.list_waste_types()
    areas = wr_srv.list_recycling_areas()
    users = user_srv.list_users(get_all=True)

    take_order_dict = {x.order_id: x for x in t_orders}
    tps_dict = {x.id: x for x in waste_tps}
    area_dict = {x.id: x for x in areas}
    user_dict = user_srv.list_to_dict(users)
    datas: list[RecyclingOrderData] = [RecyclingOrderData(
        x,
        take_order_dict.get(x.id),
        tps_dict.get(x.waste_type_id),
        area_dict.get(x.recycling_area_id),
        user_dict.get(take_order_dict.get(x.id).ragman_id),
    ) for x in orders]

    return HTTPHelper.generate_response(data=ListRecyclingOrdersResponseData(datas).to_dict())


@recycling_bp.route('/order/take', methods=['POST'])
@jwt_required()
@logger.catch
def take_order():
    form = TakeOrdersRequestForm()
    if not form.validate():
        return form.arguments_error()

    return HTTPHelper.generate_response(
        code=wr_srv.batch_take_order(form.get_recycling_order_ids(), ragman_id=current_user.id))


@recycling_bp.route('/order/exec', methods=['POST'])
@jwt_required()
@logger.catch
def update_order_execution_info():
    form = UpdateOrderExecutionInfoRequestForm()
    if not form.validate():
        return form.arguments_error()

    return HTTPHelper.generate_response(code=wr_srv.update_order_execution_info(form))


@dashboard_bp.route('/detail', methods=['GET'])
@jwt_required()
@logger.catch
def get_dashboard():
    # 废品今日价格
    waste_today_price = {
        '玻璃': 0.2,
        '纸类': 1.2,
        '衣服类': 3,
        '塑料': 3,
        '铁': 10.2,
    }

    # 近七天分类废品成交数扇形图
    fan_chart_data = {
        '生活区域': '36%',
        '教学区域': '25%',
        '后勤区域': '24%',
        '办公区域': '15%',
        '其他区域': '10%'
    }

    # 上周各物品价格走势图
    last_week_trend_data = {
        '星期一': [['玻璃', 0.2], ['衣服类', 1], ['纸类', 1.3], ['铁', 2], ['塑料', 2.6]],
        '星期二': [['玻璃', 0.35], ['衣服类', 1.5], ['纸类', 1.2], ['铁', 2.4], ['塑料', 2.7]],
        '星期三': [['玻璃', 0.25], ['衣服类', 2], ['纸类', 1.4], ['铁', 2.3], ['塑料', 2.8]],
        '星期四': [['玻璃', 0.19], ['衣服类', 1.3], ['纸类', 1.5], ['铁', 2.6], ['塑料', 2.95]],
        '星期五': [['玻璃', 0.21], ['衣服类', 1.9], ['纸类', 1.6], ['铁', 3], ['塑料', 3.2]],
        '星期六': [['玻璃', 0.22], ['衣服类', 2], ['纸类', 1.7], ['铁', 2.5], ['塑料', 3.1]],
        '星期日': [['玻璃', 0.24], ['衣服类', 1.7], ['纸类', 1.4], ['铁', 2.4], ['塑料', 3]],
    }

    # 各物品上周成交量(单位：kg)与成交额(单位：元)
    # 种类 成交量 成交额
    multi_bar_chart_data = {
        '星期一': [['玻璃', 15, 120.3], ['衣服类', 62.5, 100.5], ['纸类', 90, 1500], ['铁', 25, 300],
                   ['塑料', 32, 780]],
        '星期二': [['玻璃', 18, 115.6], ['衣服类', 65, 102.2], ['纸类', 85, 1480], ['铁', 28, 310], ['塑料', 30, 790]],
        '星期三': [['玻璃', 20, 122.8], ['衣服类', 70, 98.5], ['纸类', 88, 1520], ['铁', 30, 320], ['塑料', 35, 810]],
        '星期四': [['玻璃', 16, 118.5], ['衣服类', 60, 99.9], ['纸类', 92, 1550], ['铁', 26, 305], ['塑料', 28, 770]],
        '星期五': [['玻璃', 22, 124.7], ['衣服类', 68, 105.3], ['纸类', 87, 1495], ['铁', 31, 315], ['塑料', 33, 785]],
        '星期六': [['玻璃', 19, 120.9], ['衣服类', 63, 101.8], ['纸类', 91, 1535], ['铁', 27, 312], ['塑料', 31, 795]],
        '星期日': [['玻璃', 17, 116.4], ['衣服类', 65, 103.6], ['纸类', 89, 1510], ['铁', 29, 318], ['塑料', 34, 800]],
    }

    # 近七天具体地点的废品总成交数柱状图
    bar_chart_data = {
        '产教融合大楼': 319,
        '公教楼': 246,
        '图书馆': 102,
        '音美体楼': 431,
        '宿舍楼': 68,
        '后勤楼': 201,
        '第一食堂': 423,
        '第二食堂': 189,
        '理科楼': 55,
        '文科楼': 297,
        '行政楼': 74,
        '操场': 416
    }

    waste_today_price = [WasteTypeTodayPriceData(k, v) for k, v in waste_today_price.items()]
    fan_chart = [FanChartData(k, v) for k, v in fan_chart_data.items()]
    bar_chart = [BarChartData(k, v) for k, v in bar_chart_data.items()]
    last_week_trend = []
    multi_bar_chart = []
    for k, v in last_week_trend_data.items():
        items = [WasteTypePriceInfoData(x[0], x[1]) for x in v]
        last_week_trend.append(LastWeekTrendData(k, items))

    for k, v in multi_bar_chart_data.items():
        items = [MultiBarChartItemData(x[0], x[1], x[2]) for x in v]
        multi_bar_chart.append(MultiBarChartData(k, items))

    return HTTPHelper.generate_response(data=GetDashboardResponseData(
        waste_type_today_prices=waste_today_price,
        last_week_trend_datas=last_week_trend,
        fan_chart_datas=fan_chart,
        bar_chart_datas=bar_chart,
        multi_bar_chart_data=multi_bar_chart,
    ).to_dict())
