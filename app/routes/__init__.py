from flask_jwt_extended import JWTManager
from loguru import logger
from .user import user_bp
from .role import role_bp
from .waste_recycling import area_bp, waste_type_bp, recycling_bp, dashboard_bp

from app.services import user_srv, cache
from app.utils.httpx import HTTPHelper
from app.utils.errcode import ErrCode

jwt = JWTManager()


def register_routes(app):
    jwt.init_app(app)
    app.register_blueprint(user_bp, url_prefix='/api/wrs/v1/user')
    app.register_blueprint(role_bp, url_prefix='/api/wrs/v1/role')
    app.register_blueprint(area_bp, url_prefix='/api/wrs/v1/recyclingArea')
    app.register_blueprint(waste_type_bp, url_prefix='/api/wrs/v1/wasteType')
    app.register_blueprint(recycling_bp, url_prefix='/api/wrs/v1/recycling')
    app.register_blueprint(dashboard_bp, url_prefix='/api/wrs/v1/dashboard')
    app.register_blueprint(app.config["SWAGGER_UI_BLUEPRINT"], url_prefix=app.config["SWAGGER_URL"])

    @app.errorhandler(Exception)
    def handle_exception(e):
        return HTTPHelper.generate_response(code=ErrCode.ERROR_INTERNAL_SERVER_ERROR, msg=str(e))


@jwt.unauthorized_loader
def unauthorized_callback(_error):
    return HTTPHelper.generate_response(code=ErrCode.ERROR_AUTHORIZATION_REQUIRED)


# 注册一个回调函数，该函数在使用 create_access_token 创建 JWT 时将传入的任何对象作为身份，并将其转换为 JSON 可序列化格式。
@jwt.user_identity_loader
def user_identity_lookup(current_user: int) -> int:
    logger.debug(f"user_identity_lookup, current_user: {current_user}", )
    return current_user


# 注册一个回调函数，在访问受保护的路由时从数据库自动加载用户（current_user）。
# 这应该在成功查找时返回任何 python 对象，或者如果查找因任何原因失败（例如，如果用户已从数据库中删除）则返回 None。
@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    logger.debug(f'user_lookup_callback, _jwt_header: {_jwt_header}, jwt_data: {jwt_data}')
    identity: int = jwt_data["sub"]
    return user_srv.get_by_id(identity)


@jwt.user_lookup_error_loader
def default_user_lookup_error_callback(_jwt_header, _jwt_data):
    return HTTPHelper.generate_response(code=ErrCode.ERROR_TOKEN_INVALID)


# 回调函数用来检查 jwt 是否已经存在 redis 的黑名单列表之中
@jwt.token_in_blocklist_loader
def check_if_token_is_revoked(_jwt_header, jwt_payload: dict):
    jti = jwt_payload["jti"]
    return cache.exists(jti)


@jwt.additional_claims_loader
def add_claims_to_jwt(identity: int):
    r = user_srv.get_role_by_user_id(identity)
    if r is None:
        return {'is_admin': False}
    if r.role_code == "admin":
        return {'is_admin': True}
    return {'is_admin': False}


@jwt.expired_token_loader
def expired_token_callback(_jwt_header, _jwt_payload):
    return HTTPHelper.generate_response(code=ErrCode.ERROR_TOKEN_EXPIRED)


@jwt.invalid_token_loader
def invalid_token_callback(_error):  # we have to keep the argument here, since it's passed in by the caller internally
    return HTTPHelper.generate_response(code=ErrCode.ERROR_TOKEN_INVALID)


# 定义撤销 Token 回调函数
@jwt.revoked_token_loader
def revoked_token_callback(_jwt_header, _jwt_payload):
    return HTTPHelper.generate_response(code=ErrCode.ERROR_TOKEN_REVOKED)
