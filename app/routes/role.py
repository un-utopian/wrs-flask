from flask import Blueprint
from flask_jwt_extended import jwt_required
from loguru import logger

from .resp_data import ListRolesData, RoleData
from app.forms.forms import *
from app.services import auth_srv
from app.utils.httpx import HTTPHelper

# 创建蓝图
role_bp = Blueprint('role', __name__)


@role_bp.route('/list', methods=['GET'])
@jwt_required()
@logger.catch
def list_roles():
    roles = auth_srv.list_roles()
    datas: list[RoleData] = []
    for v in roles:
        datas.append(RoleData(v))

    return HTTPHelper.generate_response(data=ListRolesData(datas).to_dict())
