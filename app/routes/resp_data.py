from app.models.user import *
from app.models.waste_recycling import *
from datetime import datetime


class RespData:
    def __init__(self):
        pass

    def to_dict(self):
        return self.__dict__

    @staticmethod
    def datetime_format(dt: datetime):
        if isinstance(dt, datetime):
            return dt.strftime("%Y-%m-%d %H:%M:%S")
        return ""

    @staticmethod
    def fen_to_yuan(price_fen):
        """
        将价格从人民币（单位为分）转换成人民币（元）

        参数：
        price_fen (int): 以分为单位的价格

        返回：
        float: 以元为单位的价格
        """
        return price_fen / 100.0

    @staticmethod
    def gram_to_kilogram(weight_gram):
        """
        将重量从克转换成千克

        参数：
        weight_gram (float): 以克为单位的重量

        返回：
        float: 以千克为单位的重量
        """
        return weight_gram / 1000.0


class LoginData(RespData):
    def __init__(self, user_id, token, role_code):
        super().__init__()
        self.user_id = user_id
        self.token = token
        self.role_code = role_code


class RegisterData(LoginData):
    pass


class RoleData(RespData):
    def __init__(self, role: WrsRole):
        super().__init__()
        self.id = 0
        self.name = ''
        self.code = ''
        if role is not None:
            self.id = role.id
            self.name = role.role_name
            self.code = role.role_code


class UserData(RespData):
    def __init__(self, user: WrsUser, role: WrsRole):
        super().__init__()
        self.id = 0
        self.username = ''
        self.email = ''
        self.create_time = ''
        if role is not None:
            self.role = RoleData(role).to_dict()
        if user is not None:
            self.id = user.id
            self.username = user.username
            self.email = user.email
            self.create_time = self.datetime_format(user.create_time)

    def set_role(self, role_data: RoleData):
        self.role = role_data.to_dict() if role_data is not None else None


class ListUsersData(RespData):
    def __init__(self, users: list[UserData]):
        super().__init__()
        self.users = [user.to_dict() for user in users]


class ListRolesData(RespData):
    def __init__(self, roles: list[RoleData]):
        super().__init__()
        self.roles = [role.to_dict() for role in roles]


class WasteTypeData(RespData):
    def __init__(self, waste_type: WrsWasteType, waste_price: WrsWasteQuotationPrice = None):
        super().__init__()
        self.id = 0
        self.waste_type_name = ""
        self.s_today_price = 0.0
        self.today_price = 0

        if waste_type:
            self.id = waste_type.id
            self.waste_type_name = waste_type.waste_type_name
        if waste_price:
            self.s_today_price = self.fen_to_yuan(waste_price.recycling_price)
            self.today_price = waste_price.recycling_price


class ListWasteTypesResponseData(RespData):
    def __init__(self, waste_types: list[WasteTypeData]):
        super().__init__()
        self.waste_types = [wt.to_dict() for wt in waste_types]


class RecyclingAreaData(RespData):
    def __init__(self, area: WrsRecyclingArea):
        super().__init__()
        self.id = 0
        self.recycling_area_name = ""
        self.recycling_area_type = 0

        if area:
            self.id = area.id
            self.recycling_area_name = area.area_name
            self.recycling_area_type = area.area_type


class ListRecyclingAreaData(RespData):
    def __init__(self, areas: list[RecyclingAreaData]):
        super().__init__()
        self.recycling_areas = [v.to_dict() for v in areas]


class RecyclingOrderData(RespData):
    def __init__(self,
                 recycling_order: WrsRecyclingOrder,
                 take_order: WrsTakeOrder,
                 waste_type: WrsWasteType,
                 area: WrsRecyclingArea,
                 ragman: WrsUser):
        super().__init__()
        self.username = ""
        self.phone = ""
        self.appointment_time = ""
        self.recycling_area_id = 0
        self.waste_type_id = 0
        self.address = ""
        self.remark = ""
        self.status = 0
        self.ragman_id = 0
        self.ragman_name = ""
        self.transaction_price = 0
        self.s_transaction_price = 0.0
        self.turnover = 0
        self.s_turnover = 0.0
        self.create_time = ""
        self.completion_time = ""
        self.id = 0
        self.recycling_area_name = ""
        self.waste_type_name = ""

        if recycling_order:
            self.username = recycling_order.username
            self.phone = recycling_order.phone
            self.appointment_time = self.datetime_format(recycling_order.appointment_time)
            self.recycling_area_id = recycling_order.recycling_area_id
            self.waste_type_id = recycling_order.waste_type_id
            self.address = recycling_order.address
            self.remark = recycling_order.remark
            self.status = recycling_order.status
            self.create_time = self.datetime_format(recycling_order.create_time)
            self.id = recycling_order.id
        if take_order:
            self.ragman_id = take_order.ragman_id
            self.transaction_price = take_order.transaction_price
            self.s_transaction_price = self.fen_to_yuan(take_order.transaction_price)
            self.turnover = take_order.turnover
            self.s_turnover = self.gram_to_kilogram(take_order.turnover)
            self.completion_time = self.datetime_format(take_order.completion_time)
        if ragman:
            self.ragman_name = ragman.username
        if area:
            self.recycling_area_name = area.area_name
        if waste_type:
            self.waste_type_name = waste_type.waste_type_name


class ListRecyclingOrdersResponseData(RespData):
    def __init__(self, recycling_orders: list[RecyclingOrderData]):
        super().__init__()
        self.recycling_orders = [ro.to_dict() for ro in recycling_orders]


# 今日废品单价
class WasteTypeTodayPriceData(RespData):
    def __init__(self, waste_type_name, univalence):
        super().__init__()
        self.waste_type_name = waste_type_name
        self.univalence = univalence


# 废品信息价格
class WasteTypePriceInfoData(RespData):
    def __init__(self, waste_type_name, price):
        super().__init__()
        self.waste_type_name = waste_type_name
        self.price = price


class LastWeekTrendData(RespData):
    def __init__(self, week_day, waste_type_price_infos: list[WasteTypePriceInfoData]):
        super().__init__()
        self.week_day = week_day
        self.waste_type_price_infos = [wt.to_dict() for wt in waste_type_price_infos]


class FanChartData(RespData):
    def __init__(self, recycling_area_name, percentage):
        super().__init__()
        self.recycling_area_name = recycling_area_name
        self.percentage = percentage


class BarChartData(RespData):
    def __init__(self, recycling_area_name, transaction_num):
        super().__init__()
        self.recycling_area_name = recycling_area_name
        self.transaction_num = transaction_num


class MultiBarChartItemData(RespData):
    def __init__(self, waste_type_name, waste_turnover, transaction_price):
        super().__init__()
        self.waste_type_name = waste_type_name
        self.waste_turnover = waste_turnover
        self.transaction_price = transaction_price


class MultiBarChartData(RespData):
    def __init__(self, week_day, items: list[MultiBarChartItemData]):
        super().__init__()
        self.week_day = week_day
        self.items = [v.to_dict() for v in items]


class GetDashboardResponseData(RespData):
    def __init__(self,
                 waste_type_today_prices: list[WasteTypeTodayPriceData],
                 last_week_trend_datas: list[LastWeekTrendData],
                 fan_chart_datas: list[FanChartData],
                 bar_chart_datas: list[BarChartData],
                 multi_bar_chart_data: list[MultiBarChartData],
                 ):
        super().__init__()
        self.waste_type_today_prices = [wt.to_dict() for wt in waste_type_today_prices]
        self.last_week_trend_datas = [lw.to_dict() for lw in last_week_trend_datas]
        self.fan_chart_datas = [fc.to_dict() for fc in fan_chart_datas]
        self.bar_chart_datas = [bc.to_dict() for bc in bar_chart_datas]
        self.multi_bar_chart_data = [mb.to_dict() for mb in multi_bar_chart_data]
