from flask import Blueprint
from flask_jwt_extended import jwt_required, create_access_token, get_jwt, current_user
from loguru import logger

from .resp_data import *
from app.config import JWT_ACCESS_TOKEN_EXPIRES
from app.forms.forms import *
from app.models.enum import RoleCode
from app.services import user_srv, cache
from app.utils.httpx import HTTPHelper
from app.utils.errcode import ErrCode
from app.utils.password import Password

# 创建蓝图
user_bp = Blueprint('user', __name__)


@user_bp.route('/register', methods=['POST'])
@logger.catch
def register():
    register_form = RegisterForm()
    if not register_form.validate():
        return register_form.arguments_error()
    username = register_form.get_username()
    em = register_form.get_email()
    password = register_form.get_password()
    phone = register_form.get_phone()
    role_code = register_form.get_role_code()
    logger.debug(f'username: {username}, email: {em}, phone: {phone}')

    # 校验邮箱或手机是否已经被使用
    if user_srv.exist_by_uni_info(em, phone):
        return HTTPHelper.generate_response(code=ErrCode.ERROR_EMAIL_OR_PHONE_ALREADY_EXISTS)

    if role_code == "":
        role_code = RoleCode.ORDINARY_USER.value
    user = user_srv.create(username, em, Password.encrypt(password), phone, role_code=role_code)
    if user is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_INTERNAL_SERVER_ERROR)

    token = create_access_token(identity=user.id)
    return HTTPHelper.generate_response(data=RegisterData(user.id, token, role_code).to_dict())


@user_bp.route('/login', methods=['POST'])
@logger.catch
def login():
    login_form = LoginForm()
    if not login_form.validate():
        return login_form.arguments_error()
    em = login_form.get_email()
    phone = login_form.get_phone()
    password = login_form.get_password()

    # 校验用户是否存在
    user = user_srv.get_by_uni_info(em, phone)
    if not user:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_USER_NOT_EXISTS)

    if not Password.verify(password, user.password):
        return HTTPHelper.generate_response(code=ErrCode.ERROR_WRONG_PASSWORD, data={})

    # 获取用户角色
    role = user_srv.get_role_by_user_id(user.id)
    identity = user.id
    token = create_access_token(identity=identity)
    return HTTPHelper.generate_response(
        data=LoginData(user.id, token, role.role_code if role is not None else RoleCode.ORDINARY_USER.value).to_dict())


@user_bp.route("/logout", methods=["POST"])
@jwt_required()
@logger.catch
def logout():
    jti = get_jwt()["jti"]
    cache.set(jti, "", ex=JWT_ACCESS_TOKEN_EXPIRES)
    return HTTPHelper.generate_response()


@user_bp.route("/create", methods=["POST"])
@jwt_required()
def create_user():
    create_user_form = CreateUserForm()
    if not create_user_form.validate():
        return create_user_form.arguments_error()
    username = create_user_form.get_username()
    em = create_user_form.get_email()
    phone = create_user_form.get_phone()
    password = create_user_form.get_password()
    role_code = create_user_form.get_role_code()

    # 校验邮箱是否已经被使用
    if user_srv.exist_by_uni_info(em, phone):
        return HTTPHelper.generate_response(code=ErrCode.ERROR_EMAIL_OR_PHONE_ALREADY_EXISTS)

    user = user_srv.create(username, em, Password.encrypt(password), phone, role_code)
    if user is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_INTERNAL_SERVER_ERROR)

    return HTTPHelper.generate_response(data={})


@user_bp.route("/delete", methods=["POST"])
@jwt_required()
def delete_user():
    user_id_form = UserIDForm()
    if not user_id_form.validate():
        return user_id_form.arguments_error()
    user_id = user_id_form.get_user_id()
    user_srv.delete(user_id)
    return HTTPHelper.generate_response()


@user_bp.route("/password/reset", methods=["POST"])
@jwt_required()
def reset_password():
    user_id_form = UserIDForm()
    if not user_id_form.validate():
        return user_id_form.arguments_error()
    user_id = user_id_form.get_user_id()
    user = user_srv.get_by_id(user_id)
    user.password = Password.encrypt("123456")
    if user is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_USER_NOT_EXISTS)
    if user.save() <= 0:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_INTERNAL_SERVER_ERROR)
    return HTTPHelper.generate_response(data={'password': "123456"})


@user_bp.route("/password/modify", methods=["POST"])
@jwt_required()
def modify_password():
    mf = ModifyPasswordForm()
    if not mf.validate():
        return mf.arguments_error()
    user_id = mf.get_user_id()
    old_password = mf.get_old_password()
    new_password = mf.get_new_password()
    if old_password == new_password:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_VALIDATION_ERROR, msg="两次密码不能一样")

    user = user_srv.get_by_id(user_id)
    if user is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_USER_NOT_EXISTS)

    if not Password.verify(old_password, user.password):
        return HTTPHelper.generate_response(code=ErrCode.ERROR_WRONG_PASSWORD)

    user.password = Password.encrypt(new_password)
    if user.save() <= 0:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_INTERNAL_SERVER_ERROR)

    jti = get_jwt()["jti"]
    cache.set(jti, "", ex=JWT_ACCESS_TOKEN_EXPIRES)
    return HTTPHelper.generate_response()


@user_bp.route("/role/update", methods=["POST"])
@jwt_required()
def update_user_role():
    uf = UpdateUserRoleForm()
    if not uf.validate():
        return uf.arguments_error()
    user_id = uf.get_user_id()
    role_id = uf.get_role_id()

    user = user_srv.get_by_id(user_id)
    if user is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_USER_NOT_EXISTS)

    if user_srv.update_user_role(user.id, role_id) <= 0:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_INTERNAL_SERVER_ERROR)
    return HTTPHelper.generate_response()


@user_bp.route("/list", methods=["GET"])
@jwt_required()
@logger.catch
def list_users():
    pg = ListUsersForm()
    if not pg.validate():
        return pg.arguments_error()
    role_id = pg.get_role_id()
    fc = None
    if role_id > 0:
        users_roles = user_srv.list_users_roles(role_id=pg.get_role_id())
        fc = {"user_ids": [v.user_id for v in users_roles]}

    users = user_srv.list_users(fc=fc)
    user_ids = [user.id for user in users]
    user_role_dict = user_srv.get_user_role_dict(user_ids)
    datas = [UserData(user, user_role_dict.get(user.id)) for user in users]
    return HTTPHelper.generate_response(data=ListUsersData(datas).to_dict())


@user_bp.route("/detail", methods=["GET"])
@jwt_required()
@logger.catch
def get_user():
    uf = UserIDForm()
    if not uf.validate():
        return uf.arguments_error()
    user_id = uf.get_user_id()

    user = user_srv.get_by_id(user_id)
    if user is None:
        return HTTPHelper.generate_response(code=ErrCode.ERROR_USER_NOT_EXISTS)
    role = user_srv.get_role_by_user_id(user.id)
    return HTTPHelper.generate_response(data=UserData(user, role).to_dict())


@user_bp.route("/protected", methods=["GET"])
@jwt_required()
def protected():
    # 我们现在可以通过 `current_user` 访问我们的 sqlalchemy User 对象。
    return HTTPHelper.generate_response(data={
        'id': current_user.id,
        'email': current_user.email
    })
