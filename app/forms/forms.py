from wtforms import Form, StringField, IntegerField, FieldList
from wtforms.validators import *
from app.utils.httpx import HTTPHelper
from app.utils.errcode import ErrCode
from flask import request
from app.models.enum import RoleCode


# 自定义请求
class RequestForm(Form):
    # 初始化参数
    def __init__(self):
        content_type = request.headers.get("Content-Type")

        # 获取 “application/json” 请求
        if content_type and "application/json" in content_type:
            data = request.get_json(silent=True)
            args = request.args.to_dict()
            super(RequestForm, self).__init__(data=data, **args)
        else:
            # 获取 “application/x-www-form-urlencoded” 或者 “multipart/form-data” 请求
            data = request.form.to_dict()
            args = request.args.to_dict()
            super(RequestForm, self).__init__(data=data, **args)

    def validate_arguments(self):
        # 验证参数
        valid = super(RequestForm, self).validate()

        # 验证参数失败
        if not valid:
            # 此处可以写错误返回值
            print("field is require")

        return self.validate()

    def arguments_error(self):
        return HTTPHelper.generate_response(
            data=self.errors,
            code=ErrCode.ERROR_VALIDATION_ERROR,
        )


class PaginatorForm(RequestForm):
    page = IntegerField('page', validators=[DataRequired()])
    size = IntegerField('size', validators=[DataRequired()])

    def get_page(self):
        return self.page.data

    def get_size(self):
        return self.size.data


class LoginForm(RequestForm):
    phone = StringField('phone', validators=[Length(min=0, max=32)])
    email = StringField('email', validators=[Length(min=0, max=32)])
    password = StringField('password', validators=[Length(min=6, max=20), DataRequired()])

    def get_email(self):
        return self.email.data

    def get_password(self):
        return self.password.data

    def get_phone(self):
        return self.phone.data


class RegisterForm(RequestForm):
    username = StringField('username', [Length(min=2, max=25)])
    email = StringField('email', validators=[Length(min=6, max=32), Email()])
    password = StringField('password', validators=[Length(min=6, max=20), DataRequired()])
    phone = StringField('phone', validators=[DataRequired()])
    role_code = StringField('role_code')

    def get_username(self):
        return self.username.data

    def get_email(self):
        return self.email.data

    def get_password(self):
        return self.password.data

    def get_phone(self):
        return self.phone.data

    def get_role_code(self):
        return self.role_code.data


class CreateUserForm(RegisterForm):
    # 必须在 几个string 的枚举值中
    role_code = StringField('role_code', [AnyOf([v.value for v in RoleCode]), DataRequired()])

    def get_role_code(self):
        return self.role_code.data


class UserIDForm(RequestForm):
    user_id = IntegerField('user_id', validators=[DataRequired(), NumberRange(min=1)])

    def get_user_id(self):
        return self.user_id.data


class ModifyPasswordForm(UserIDForm):
    old_password = StringField('old_password', validators=[Length(min=6, max=20), DataRequired()])
    new_password = StringField('new_password', validators=[Length(min=6, max=20), DataRequired()])

    def get_old_password(self):
        return self.old_password.data

    def get_new_password(self):
        return self.new_password.data


class UpdateUserRoleForm(UserIDForm):
    role_id = IntegerField('role_id', validators=[DataRequired(), NumberRange(min=1)])

    def get_role_id(self):
        return self.role_id.data


class ListUsersForm(RequestForm):
    role_id = IntegerField('role_id', default=0)

    def get_role_id(self):
        return self.role_id.data


class WasteTypePriceForm(RequestForm):
    today_price = IntegerField('today_price', validators=[DataRequired(), NumberRange(min=0)])
    waste_type_id = IntegerField('waste_type_id', validators=[DataRequired(), NumberRange(min=1)])

    def get_today_price(self):
        return self.today_price.data

    def get_waste_type_id(self):
        return self.waste_type_id.data


class RecyclingOrderIDForm(RequestForm):
    recycling_order_id = IntegerField('recycling_order_id', validators=[DataRequired(), NumberRange(min=1)])

    def get_recycling_order_id(self):
        return self.recycling_order_id.data


class CreateRecyclingOrderForm(RequestForm):
    username = StringField('username', validators=[DataRequired(), Length(max=255)])
    phone = StringField('phone', validators=[DataRequired(), Length(max=255)])
    appointment_time = StringField('appointment_time', validators=[DataRequired(), Length(max=255)])
    recycling_area_id = IntegerField('recycling_area_id', validators=[DataRequired(), NumberRange(min=1)])
    waste_type_id = IntegerField('waste_type_id', validators=[DataRequired(), NumberRange(min=1)])
    address = StringField('address', validators=[DataRequired(), Length(max=255)])
    remark = StringField('remark')

    def get_username(self):
        return self.username.data

    def get_phone(self):
        return self.phone.data

    def get_appointment_time(self):
        return self.appointment_time.data

    def get_recycling_area_id(self):
        return self.recycling_area_id.data

    def get_waste_type_id(self):
        return self.waste_type_id.data

    def get_address(self):
        return self.address.data

    def get_remark(self):
        return self.remark.data


class UpdateRecyclingOrderForm(RecyclingOrderIDForm, CreateRecyclingOrderForm):
    remark = StringField('remark', validators=[DataRequired(), Length(max=255)])
    ragman_id = IntegerField('ragman_id', validators=[NumberRange(min=0)])
    status = IntegerField('status', validators=[DataRequired(), AnyOf([1, 2, 3, 4])])
    transaction_price = IntegerField('transaction_price', validators=[NumberRange(min=0)])
    turnover = IntegerField('turnover', validators=[NumberRange(min=0)])

    def get_ragman_id(self):
        return self.ragman_id.data

    def get_status(self):
        return self.status.data

    def get_transaction_price(self):
        return self.transaction_price.data

    def get_turnover(self):
        return self.turnover.data

    # 超管修改字段
    def get_remark(self):
        return self.remark.data


class TakeOrdersRequestForm(RequestForm):
    recycling_order_ids = FieldList(IntegerField('recycling_order_ids'), validators=[DataRequired()])

    def get_recycling_order_ids(self):
        return self.recycling_order_ids.data


class UpdateOrderExecutionInfoRequestForm(RequestForm):
    recycling_order_id = IntegerField('recycling_order_id', validators=[DataRequired(), NumberRange(min=1)])
    status = IntegerField('status', validators=[DataRequired(), AnyOf([3, 4])])
    transaction_price = IntegerField('transaction_price')
    turnover = IntegerField('turnover')

    def get_recycling_order_id(self):
        return self.recycling_order_id.data

    def get_status(self):
        return self.status.data

    def get_transaction_price(self):
        return self.transaction_price.data

    def get_turnover(self):
        return self.turnover.data
