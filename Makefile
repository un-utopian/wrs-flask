.PHONY: init
# init env
init:
	go install github.com/google/gnostic/cmd/protoc-gen-openapi@latest # protoc-gen-openapi
# openapi
.PHONY: openapi
# generate api
openapi:
	protoc --proto_path=. \
		   --proto_path=./third_party \
		   --openapi_out=fq_schema_naming=true,default_response=false:app/static/ \
		   api/wrs/v1/api.proto
